*** Settings ***
Documentation  Example for using an external database library,
...            writing a test suite for an ETL job and using variable files.
Library  DatabaseLibrary
Library  OperatingSystem

Test Setup  Prepare Database


*** Variables ***
${job_location}  C:/demo/jobs/pg_robot/pg_robot/
${table_name}    mytest


*** Test Cases ***
Example
  Connect To Database    dbapiModuleName=psycopg2    dbName=robot    dbUsername=postgres    dbPassword=postgres    dbHost=localhost    dbPort=5432
  Run Keyword And Ignore Error    Execute Sql String    DROP SCHEMA workshop CASCADE
  Execute Sql String   CREATE SCHEMA workshop
  ${res}=  Query  SELECT schema_name FROM information_schema.schemata where schema_name = 'workshop'
  Log  ${res}[0]
  Should Be Equal    workshop    ${res}[0][0]
  Disconnect From Database

Check Variables
  Log Variables

Database Test
  Connect To Database    dbapiModuleName=psycopg2    dbName=${pg_dbName}    dbUsername=${pg_dbUsername}    dbPassword=${pg_dbPassword}    dbHost=${pg_dbHost}    dbPort=${pg_dbPort}
  Row Count Is Equal To X  SELECT * FROM workshop.${table_name}  1
  ${res}=  Query  SELECT * FROM workshop.${table_name}
  Disconnect From Database
  Should Be Equal  ${res}[0][0]  Mr
  Should Be Equal  ${res}[0][1]  Robot
  Should Be Equal  ${res}[0][2]  Finland

Load Data To Stage
  Connect To Database    dbapiModuleName=psycopg2    dbName=${pg_dbName}    dbUsername=${pg_dbUsername}    dbPassword=${pg_dbPassword}    dbHost=${pg_dbHost}    dbPort=${pg_dbPort}
  Delete All Rows From Table  workshop.${table_name}
  ${rc}  ${output}=  Run And Return Rc And Output  ${job_location}pg_robot_run.bat --context_param pg_tablename=${table_name}
  Should Be Equal As Integers  ${rc}  0
  Row Count Is Equal To X  SELECT * FROM workshop.${table_name}  12
  Check If Exists In Database  SELECT * FROM workshop.${table_name} WHERE name='Tom' AND surname = 'Sawyer' AND state='Wyoming'
  Check If Exists In Database  SELECT * FROM workshop.${table_name} WHERE name='Mark' AND surname = 'Twain' AND state='California'
  Disconnect From Database


*** Keywords ***
Prepare Database
  Connect To Database    dbapiModuleName=psycopg2    dbName=${pg_dbName}    dbUsername=${pg_dbUsername}    dbPassword=${pg_dbPassword}    dbHost=${pg_dbHost}    dbPort=${pg_dbPort}
  Execute Sql String  CREATE SCHEMA IF NOT EXISTS workshop
  Execute Sql String  DROP TABLE IF EXISTS workshop.${table_name}
  Execute Sql String  CREATE TABLE workshop.${table_name} (name varchar, surname varchar, state varchar)
  Execute Sql String  INSERT INTO workshop.${table_name} VALUES ('Mr', 'Robot', 'Finland')
  ${res}=  Query  SELECT * FROM workshop.${table_name}
  Disconnect From Database