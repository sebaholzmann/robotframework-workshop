# Robot Framework Workshop

## Excercises

### Custom Libraries 1
**Prerequisites:**
* --

**In this excercise you will learn:**
* how to write a custom library in python
* extending pythonpath to use the library (commandline option: *--pythonpath*)
* generate documentation for your libraries with libdoc

**Excercise:**
1. Open Custom_Libraries_1_Examples.robot
2. Inspect and run examples. Why does the test suite fail? 
3. Your command might be quite long (something like `robot -d logs --variablefile .\config\var_postgres.py --exclude ignore .\test-suites\`). Create a `run` folder with file `args.txt` and outsource all commandline options:

        --outputdir logs
        --exclude ignore
        --noncritical not_important
        --variablefile config/var_postgres.py
        --pythonpath libraries

3.  
   1. Write a python keyword which takes two strings as argument and concats them with pip-separator (|).
   2. Write a python keyword that counts the number of words in a given text file. Verify your result 
    
    *hint:*

        https://www.sanfoundry.com/python-program-count-number-words-characters-file/ 

      * The keyword should log the number of words with info loglevel
      
        *hint:* 
        * Logging - Programmatic Loggin APIs:
        ``` http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#logging-information ```     -> 
4. Generate the documentation for your own library
       
        python -m robot.libdoc libraries/CustomPython.py documentation/CustomPython.html

5. Open Exasol.py and compare to your own library
6. Generate the documentation for Exasol.py
   
        python -m robot.libdoc libraries/Exasol.py documentation/Exasol.html

*hints:*
* args.txt: add python path: *--pythonpath libraries* (commandline options: http://robotframework.org/robotframework/2.8.6/RobotFrameworkUserGuide.html#all-command-line-options)
* args.txt: add *--variable RESOURCES:./resources/*
* libdoc documentation: https://robotframework.org/robotframework/2.1.2/tools/libdoc.html
* set the python path also in the plugin settings for autocompletion



### Custom Libraries 2
**Prerequisites:**
* javaremotelib.jar started (https://cimtag.sharepoint.com/:f:/s/KTDWHBI/ErDK1zq8QKFHmsmtDqY6Em8BJXg33iTLA2tKwsj5YBrcDw?e=a45lhM)

**In this excercise you will learn:**
- using **Remote** library
- using tags to control test execution (commandline option: *--exclude*)
- optional: how to write a custom library in java as a remote library

**Excercise:**
1. Open Custom_Libraries_2_Examples.robot
2. Inspect and run examples
3. optional (time, interest and Java IDE needed ): do excercise 1a & 1b from Custom_Libraries_1_Examples.robot in java

*Documentation:*
* Remote library: java (Custom Library)
* Remote Library: https://github.com/robotframework/RemoteInterface
* JavalibCore: https://github.com/robotframework/JavalibCore/wiki/Getting-Started


*Explanation*
* **Remote** library acts as a "proxy" between Robot Framework and test libraries elsewhere (can be written in a different language like Java, Go, ...)
* **Remote** library uses XML-RPC protocol
* How To: https://blog.codecentric.de/en/2016/01/robot-framework-tutorial-2016-remote-server-keywords-in-java/