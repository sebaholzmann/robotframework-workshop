*** Settings ***
Documentation  Example of how to extend Robot Framework by your own python keyword library
...            and generating documentation for libraries.
Library  CustomPython.py
Library  String


*** Test Cases ***
Example
  ${answer}=  Return Answer    arg
  Log  ${answer}