# install folder
Contains all files needed to install robot framework and libraries needed for running test suites.

## pip
Use a 'requirements.txt' file for listing all required package names and install by executing the following command.

```sh
pip install -r requirements.txt
```